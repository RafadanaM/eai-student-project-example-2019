# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

Muhammad Rafadana Mountheira 1806241135

TODO: Write the answers related to the programming exam here.

heroku : https://adprofinal-rafadana.herokuapp.com/

Notes: 
    The app runs normally in local host but when the app is deployed, I can add a new conference but it will throw an 
    error when I view the details of the conference.
    
Task 1 :
    
    Config:
    The API key in application.properties is a configuration files so it needs to be
    set as an environment variable, not as a plain text. SO we edit the configurations in
    Intellij  and add an environment variable TEXT_ANALYTICS_API_KEY and the value is the the plain text
    We also add the variables in gitlab CI/CD.
    
    Logging:
    Logging means that  we treat logs as event stream. log provides into the behaviour of running app
    we can see in ScienceDirectPaperFinder.java we have to change System.out.println to log.error and we add
    @Log4j2 annotation on the class.
    
     
Task 2 :
    result
    ![count](images/Task2.png)