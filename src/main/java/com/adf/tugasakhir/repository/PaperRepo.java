package com.adf.tugasakhir.repository;

import java.util.List;

import com.adf.tugasakhir.dataclass.Paper;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * PaperRepo
 */
public interface PaperRepo extends JpaRepository<Paper, Long> {
    List<Paper> findByTitle(String title);
    List<Paper> findAllByOrderByIdAsc();
}